**Manifeste des règles de codage PHP (et adjacents)**
=====================================

Ce document concerne les règles de codage à suivre autour de PHP. Ces dernières s'appuient sur les standards de Symfony et plus particulièrement sur les standards définis par les documents PSR-0, PSR-1, PSR-2 et PSR-4.

---

---

#Table de contenu
[TOC]

---

---
#Exemple

Voici un exemple de code contenant la majeure partie des règles:

```php
<?php

namespace Acme;

use Mother\FooBarMother;
use Mother\FooBarInterface;

/**
 * Class FooBar.
 * 
 * @author Yoan Guillemin <yoann.guillemin@radiofrance.com>
 */
class FooBar extends FooBarMother implements FooBarInterface
{
    const SOME_CONST = 42;

    /**
     * @var string
     */
    private $fooBar;

    /**
     * FooBar constructor.
     *
     * @param string $dummy Some argument description
     */
    public function __construct($dummy)
    {
        $this->fooBar = $this->transformText($dummy);
    }

    /**
     * @return string
     *
     * @deprecated
     */
    public function someDeprecatedMethod()
    {
        @trigger_error(sprintf('The %s() method is deprecated since version 2.8 and will be removed in 3.0. Use Acme\Baz::someMethod() instead.', __METHOD__), E_USER_DEPRECATED);

        return Baz::someMethod();
    }

    /**
     * Transforms the input given as first argument.
     *
     * @param bool|string $dummy   Some argument description
     * @param array       $options An options collection to be used within the transformation
     *
     * @return string|null The transformed input
     *
     * @throws \RuntimeException When an invalid option is provided
     */
    private function transformText($dummy, array $options = array())
    {
        $defaultOptions = array(
            'some_default' => 'values',
            'another_default' => 'more values',
        );

        foreach ($options as $option) {
            if (!in_array($option, $defaultOptions)) {
                throw new \RuntimeException(sprintf('Unrecognized option "%s"', $option));
            }
        }

        $mergedOptions = array_merge(
            $defaultOptions,
            $options
        );

        if (true === $dummy) {
            return null;
        }

        if ('string' === $dummy) {
            if ('values' === $mergedOptions['some_default']) {
                return substr($dummy, 0, 5);
            }

            return ucwords($dummy);
        }
    }

    /**
     * Performs some basic check for a given value.
     *
     * @param mixed $value     Some value to check against
     * @param bool  $theSwitch Some switch to control the method's flow
     *
     * @return bool|void The resultant check if $theSwitch isn't false, void otherwise
     */
    private function reverseBoolean($value = null, $theSwitch = false)
    {
        if (!$theSwitch) {
            return;
        }

        return !$value;
    }
}
```

---

---
#Règles

---

---
##Général
###***Structure du code***

> - Ajoutez un espace après chaque virgule séparatrice
> - Ajoutez un espace autour des opérateurs `==, &&, ||`..., à l'exception de l'opérateur de concaténation `.`
> - Placez les opérateurs unaires `!, --,`... à côté des variables concernées
> - Toujours utiliser l'opérateur de comparaison identique à moins que vous n'ayez besoin de jouer avec les types
> - Utilisez les Yoda conditions, à savoir vérifier une variable à une expression, toujours placer l'expression avant la variable: `null !== $variable`. Les opérateur suivants sont concernés: `==, !=, ===, et !==`
> - Ajoutez une virgule après chaque item d'un tableau à plusieurs dimensions même après le dernier
> - Ajoutez une ligne vide avant l'expression de retour return, à moins que le retour soit seul à l'intérieur du groupe parent (comme une expression `if`)
> - Utilisez l'expression de retour `return null;` quand une fonction retourne explicitement des valeurs nulles et utilisez l'expression de retour `return;` quand une fonction retourne des valeurs vides
> - Utilisez les accolades pour indiquer les structures de contrôle quelque soit le nombre d'expressions qu'elles entourent
> - Pour les classes et les méthodes, les accolades doivent toujours être en saut de ligne après la déclaration. A contrario, celles des blocks d'instructions `for`, `foreach`, `if`, `else`, `elseif`, `switch`, `try`, `catch`, `finally`... doivent être accolés à la déclaration
> - Ajoutez un espace après toutes les structures PHP à savoir les conditions, les boucles, les blocks try/catch etc....
    >  - `if (condition)`
    >  - `switch ($foo)`
> - Les expressions ternaires `(?:)` peuvent être sur plusieurs lignes
> - La déclaration des tableaux n'est stricte à aucune méthode
> - Définissez une classe par fichier - cela ne s'applique pas à vos classes de helper privées qui ne sont pas amenées à être instanciées de l'extérieur. Elles ne sont pas concernées par les standards PSR-0 et PSR-4
> - Déclarez la classe à hériter et les classes à implémenter sur la même ligne que le nom de la classe
> - Déclarez les propriétés de vos classes avant les méthodes
> - Déclarez d'abord les méthodes publiques puis les protégées, et finalement les privées. Les exceptions de cette règle sont le constructeur de la classe, les méthodes setUp et tearDown pour les tests PHPUnit, qui doivent tout le temps être les premières méthodes afin d'améliorer la lecture des classes
> - Déclarez tous les arguments sur la même ligne que le nom des fonctions/méthodes lorsque cela est possible. Autrement, passez une ligne par arguments. Ne mettez jamais plus d'un argument par ligne dans ce cas.
> - Utilisez des parenthèses à l'instanciation des classes sans prendre en compte le nombre d'arguments des constructeurs
> - Chaque classe utilisée dans d'autres classe doit avoir son namespace déclaré dans une instruction `use` en début de fichier
> - Chaque instruction `use` doit se trouver entre le namespace de la classe et sa déclaration
> - Les exceptions et les messages d'erreurs doivent être concaténés avec la function `sprintf`
> - Appeler `trigger_error` avec un type `E_USER_DEPRECATED` doit changé en opt-in via l'opérateur `@`
> - N'utilisez pas `else`, `elseif`, `break` après les clauses `if` ou `case` qui définissent une expression de retour `return` ou  émettent une exception via l'instruction `throw`

---
###***Conventions de nommage***

> - Utilisez la syntaxe camelCase, pas d'underscore pour les variables, les fonctions et les noms/arguments de méthodes.
> - Utilisez la syntaxe snake_case pour les noms d'options et les nom de paramètres (ex: `array('foo_bar' => $fooBar)`)
> - Utilisez les namespaces pour chaque classe
> - Préfixez les noms de classes abstraites du mot clef `Abstract`
> - Suffixez les noms d'interfaces du mot clef `Interface`
> - Suffixez les noms de traits du mot clef `Trait`
> - Suffixez les noms d'exceptions du mot clef `Exception`
> - Utilisez la syntaxe camelCase pour les nom de fichiers **PHP**
> - Utilisez des caractères alphanumériques pour les noms de fichiers **PHP**
> - Pour le casting, utilisez `bool` (et non `boolean` ou `Boolean`), int (et non `integer` et float (et non `double` ou `real`)

---
##PHPDoc
###***Documentation (PHPDoc)***

> - Ajoutez un block PHPDoc pour chaque classe, méthode et fonction
> - Groupez les annotations du même type et séparez ceux de types différents par une ligne vide
> - Ne renseignez pas le tag `@return` si la méthode ne retourne rien
> - Ne pas utilisez les annotations `@package` et `@subpackage`
> - Pour le type-hinting, utilisez `bool` (et non `boolean` ou `Boolean`), int (et non `integer` et float (et non `double` ou `real`)
> - Chaque block PHPDoc de méthode/fonction doit contenir en première ligne une phrase de description se finissant par un `.`, vient ensuite les annotations `@param`, puis le `@return` et enfin l'annotation d'exception `@throws`
> - Chaque groupe d'information des annotations `@param` doivent s'aligner dans la verticale
> - Pour chaque annotation, ajoutez un espace entre les séparateurs `:` et `=`

Exemple d'un block PHPDoc d'une classe:

```php
/**
 * Petite phrase de description OBLIGATOIRE, aussi évidente soit elle.
 */
```
Exemple d'un block PHPDoc d'une propriété:
```php
/**
 * @var string
 * 
 * @Assert\NotBlank()
 * @Assert\Length(
 *      min = 2,
 *      max = 50,
 *      minMessage = "Your first name must be at least {{ limit }} characters long",
 *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
 * )
 */
private $fooBar;
```

Exemple d'un block PHPDoc d'une méthode (l'ordre des instruction est à respecter):

```php
/**
 * Transforms the input given as first argument.
 *
 * @param bool|string $dummy   Some argument description
 * @param array       $options An options collection to be used within the transformation
 *
 * @return string|null The transformed input
 *
 * @throws \RuntimeException When an invalid option is provided
 */
private function transformText($dummy, array $options = array())
```

---
##Services
###***Convention de nommage des services***

> - N'utilisez jamais l'autowiring pour les services des SDK
> - Déclarez vos alias en tête de fichier. Il en est de même pour les configurations du fichier de service courant
> - Séparez vos déclarations de services par famille grâce à un commentaire. Ex: `#### Handlers`
> - Utilisez les Séquences (saut à la ligne suivit d'un tiret `-`) plutôt que les crochets `[]` pour la déclaration des arguments/tags
> - N'utilisez pas de doubles quotes pour l'attribut `class` d'un service

---
##Routes
###***Structure***

> - Utilisez les Collections plutôt que les accolades `{ }`

---
###***Convention de nommage des routes***

> - Un nom de route se compose de plusieurs groupes séparés par des underscores `_`
> - Le premier groupe est l'alias du bundle ou de la chaine si le bundle est `AppBundle`

---
##Yaml
###***Structure***

> - Indentation avec 4 espaces

---

---
##SDK
###***Arborescence***

> - Lors du développement d'une brique SDK, l'arborescence des dossiers doit être la suivante:
```
    -> /name-php-lib
        -> /src
            -> /Bridge
            -> /WhatYouWant
            -> /docs
            -> WhatYouWant.php
            -> README.md
            -> CHANGELOG.md
        -> /tests 
```

> - Un dossier Bridge est toujours obligatoire. Il fait sens de séparer la logique du SDK et son adaptation à un framework en particulier. Le code est alors réutilisable, il suffit d'ajouter un nouveau Bridge.

---
#Autres règles

---

---
##Versioning
###***SemVer***

> - La méthodologie SemVer doit être appliquée (RTFM: https://semver.org/)
> - Une commande du ficher Makefile situé à la racine de chaque repos permet de déterminer automatiaquement la version (cf: http://psvcg.coreteks.org/php-semver-checker-git.phar)
> - Chaque Breaking Change entraine un changement de version Majeure. Il est donc appréciable de rendre le code le plus flexible possible à sa création (visibilité....)

---
###***Documentation***

> - Un fichier README.md doit être maintenu pour les informations importantes permanentes ou en fonction des versions
> - Un fichier CHANGELOG.md doit être maintenu en fonction des versions pour faciliter le passage des versions

---
##Git
###***Structure***

> - 2 branches à respecter: `develop` et `master`
> - Une branche release est tirée depuis `develop` pour être mergée sur `master` lors d'une release
> - Chaque fix d'une release se fait sur la branche de release concernée
> - Chaque branche de release mergée sur `master` doit l'être également sur `develop` pour intégrer les potentiels fix réalisés au préalable
> - La branche `master` doit être taggée selon la méthodologie semVer à chaque release
> - Un hook de pre-commit est mit en place pour s'assurer que les normes de codage soient respectées. Il exécute php-cs-fixer en --dry-run pour aider à la correction.